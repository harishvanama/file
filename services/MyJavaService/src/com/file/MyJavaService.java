package com.file;

import com.wavemaker.runtime.javaservice.JavaServiceSuperClass;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import java.io.*;

/**
 * This is a client-facing service class.  All
 * public methods will be exposed to the client.  Their return
 * values and parameters will be passed to the client or taken
 * from the client, respectively.  This will be a singleton
 * instance, shared between all requests. 
 * 
 * To log, call the superclass method log(LOG_LEVEL, String) or log(LOG_LEVEL, String, Exception).
 * LOG_LEVEL is one of FATAL, ERROR, WARN, INFO and DEBUG to modify your log level.
 * For info on these levels, look for tomcat/log4j documentation
 */
@ExposeToClient
public class MyJavaService extends JavaServiceSuperClass {
    /* Pass in one of FATAL, ERROR, WARN,  INFO and DEBUG to modify your log level;
     *  recommend changing this to FATAL or ERROR before deploying.  For info on these levels, look for tomcat/log4j documentation
     */
    public MyJavaService() {
       super(INFO);
    }

    public String sampleJavaOperation() {
       String result  = null;
       try {
          log(INFO, "Starting sample operation");
          result = "Hello World";
          log(INFO, "Returning " + result);
       } catch(Exception e) {
          log(ERROR, "The sample java service operation has failed", e);
       }
       return result;
    }
    public String  displayIt(){
 
		
 
		 System.out.println("Working Directory = " +              System.getProperty("user.dir"));
		 return this.getClass().getClassLoader().getResource("").getPath();
		 
 
	}
public String allList(String id){
    
    // Directory path here
  String path = "/root/WaveMaker/default/projects/file/src/main/webapp/resources/uploads/"+id+"/"; 
 
  String files = "";
  File folder = new File(path);
  File[] listOfFiles = folder.listFiles(); 
 String abc = "";
  for (int i = 0; i < listOfFiles.length; i++) 
  {
 
   if (listOfFiles[i].isFile()) 
   {
   files = listOfFiles[i].getName();
   System.out.println(files);
   abc += files+",";
   
      }
  }
  abc = abc.substring(0, abc.length()-1);
  return abc;
}
}
